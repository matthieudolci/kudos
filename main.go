package main

import (
	"context"
	"net/http"

	_ "expvar"
	_ "net/http/pprof"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/matthieudolci/kudos/app"
)

func main() {

	var s *app.Slack

	app.InitDb()
	defer app.DB.Close()

	ctx := context.Background()

	s, err := app.New()
	if err != nil {
		log.Fatal(err)
	}

	if err := app.Run(ctx, s); err != nil {
		log.Fatal(err)
	}

	handler, err := app.Handler(s)
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(http.ListenAndServe(":9191", handler))
}
