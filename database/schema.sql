BEGIN;

CREATE TABLE users (
    slack_userid text NOT NULL,
    slack_displayname text NOT NULL
);

CREATE TABLE kudos (
    kudo_uuid text NOT NULL,
    kudo_comment text,
    kudo_date DATE NOT NULL DEFAULT CURRENT_DATE
);

CREATE TABLE relations (
    kudo_uuid text NOT NULL,
    slack_userid_receive text NOT NULL,
    slack_userid_give text NOT NULL
);

COMMIT;
