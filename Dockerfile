FROM golang:1.11.1 AS build
ENV CGO_ENABLED=0 GOOS=linux
ADD . /go/src/gitlab.com/matthieudolci/kudos
RUN cd /go/src/gitlab.com/matthieudolci/kudos && \
    go build -installsuffix cgo -o kudos

FROM alpine:3.8
RUN apk add --no-cache ca-certificates tzdata
ENV TZ America/Los_Angeles
COPY --from=build /go/src/gitlab.com/matthieudolci/kudos/kudos /bin
CMD ["/bin/kudos"]
