package app

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/julienschmidt/httprouter"
	"github.com/nlopes/slack"
)

var months []string

// Handler ...
func Handler(s *Slack) (http.Handler, error) {

	router := httprouter.New()

	router.POST("/kudos", slackPostHandler(s))

	return router, nil
}

func slackPostHandler(s *Slack) func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		if r.URL.Path != "/kudos" {
			w.WriteHeader(http.StatusNotFound)
			_, err := w.Write([]byte(fmt.Sprintf("incorrect path: %s", r.URL.Path)))
			if err != nil {
				log.WithError(err).Error("Could not post the message incorrect path")
			}
			log.WithFields(log.Fields{"urlpath": r.URL.Path}).Error("incorrect path")
			return
		}

		if r.Body == nil {
			w.WriteHeader(http.StatusNotAcceptable)
			_, err := w.Write([]byte("empty body"))
			if err != nil {
				log.WithError(err).Error("empty body")
			}
			return
		}
		defer r.Body.Close()

		err := r.ParseForm()
		if err != nil {
			w.WriteHeader(http.StatusGone)
			_, err = w.Write([]byte("could not parse body"))
			if err != nil {
				log.WithError(err).Error("could not parse body")
			}
			return
		}

		reply := r.PostFormValue("payload")
		if len(reply) == 0 {
			w.WriteHeader(http.StatusNoContent)
			_, err = w.Write([]byte("could not find payload"))
			if err != nil {
				log.WithError(err).Error("Could not find payload")
			}
			return
		}

		var payload slack.AttachmentActionCallback

		err = json.NewDecoder(strings.NewReader(reply)).Decode(&payload)
		if err != nil {
			w.WriteHeader(http.StatusGone)
			_, err = w.Write([]byte("could not process payload"))
			if err != nil {
				log.WithError(err).Error("Could not process payload")
			}
			return
		}

		name := payload.Actions[0].Name
		uuid := strings.Join(uuids, " ")

		switch name {
		case "UserChosen":
			err = s.addUUIDToKudos(uuid)
			if err != nil {
				log.WithError(err).Error("Could not add the uuid to the kudos table")
			}

			useridGive := payload.User.ID
			userGive, err := s.Client.GetUserInfo(useridGive)
			if err != nil {
				log.WithFields(log.Fields{"userid": useridGive}).Printf("Could not grab user information.")
			}

			err = s.addUserToUsers(useridGive, userGive.Name)
			if err != nil {
				log.WithError(err).Error("Could not add the user informations to the users table")
			}

			useridReceive := payload.Actions[0].SelectedOptions[0].Value

			userReceive, err := s.Client.GetUserInfo(useridReceive)
			if err != nil {
				log.WithFields(log.Fields{"userid": useridReceive}).Printf("Could not grab user information.")
			}

			err = s.addUserToUsers(useridReceive, userReceive.Name)
			if err != nil {
				log.WithError(err).Error("Could not add the user informations to the users table")
			}

			err = s.addUserAndUUIDToRelations(uuid, useridReceive, useridGive)
			if err != nil {
				log.WithError(err).Error("Could not add the uuid and userids to the relations table")
			}

			_, err = w.Write([]byte(fmt.Sprintf("%s was selected for a kudo.", userReceive.Name)))
			if err != nil {
				log.WithError(err).Error("Could not post the selected user for kudos reply")
			}

			channelid := payload.Channel.ID

			err = s.giveMultipleKudos(uuid, useridReceive, channelid)
			if err != nil {
				log.WithError(err).Error("Could not post multiple kudos message")
			}

		case "AnotherUserChosen":
			useridReceive := payload.Actions[0].SelectedOptions[0].Value

			userReceive, err := s.Client.GetUserInfo(useridReceive)
			if err != nil {
				log.WithFields(log.Fields{"userid": useridReceive}).Printf("Could not grab user information.")
			}

			err = s.addUserToUsers(useridReceive, userReceive.Name)
			if err != nil {
				log.WithError(err).Error("Could not add the user informations to the users table")
			}

			useridGive := payload.User.ID

			err = s.addUserAndUUIDToRelations(uuid, useridReceive, useridGive)
			if err != nil {
				log.WithError(err).Error("Could not add the uuid and userids to the relations table")
			}

			_, err = w.Write([]byte(fmt.Sprintf("%s was selected for a kudo.", userReceive.Name)))
			if err != nil {
				log.WithError(err).Error("Could not post the selected user for kudos reply")
			}

			channelid := payload.Channel.ID

			err = s.giveMultipleKudos(uuid, useridReceive, channelid)
			if err != nil {
				log.WithError(err).Error("Could not post multiple kudos message")
			}

		case "NoMoreKudos":
			_, err := w.Write([]byte(fmt.Sprintf("Awesome! Let's keep going then.")))
			if err != nil {
				log.WithError(err).Error("Could not post no more kudos message")
			}

			channelid := payload.Channel.ID

			err = s.askKudosComment(channelid, uuid)
			if err != nil {
				log.WithError(err).Error("Error during kudos comment")
			}

		case "SelectResultsMonth":
			m := payload.Actions[0].SelectedOptions[0].Value
			months = append(months, m)

		case "SelectResultsYear":
			var month = &months
			year := payload.Actions[0].SelectedOptions[0].Value
			channelid := payload.Channel.ID
			m := strings.Join(*month, " ")
			err := s.generateDoc(channelid, year, m)
			if err != nil {
				log.WithError(err).Error("Error during the generation of the results document")
			}

		case "CancelKudos":
			_, err := w.Write([]byte(fmt.Sprintf("See ya later.")))
			if err != nil {
				log.WithError(err).Error("Could not post cancel kudos message")
			}
		}
		w.WriteHeader(http.StatusOK)
	}
}
