package app

import (
	"fmt"
	"strings"

	"baliance.com/gooxml/spreadsheet"
	log "github.com/Sirupsen/logrus"
	"github.com/nlopes/slack"
)

// Kudos is the db structure of a kudo
type Kudos struct {
	UUID     string `db:"kudo_uuid"`
	Comment  string `db:"kudo_comment"`
	FromName string `db:"from_name"`
	ToName   string `db:"to_name"`
	Date     string `db:"date"`
}

func (s *Slack) askForDate(ev *slack.MessageEvent) error {
	m := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[1:]
	if len(m) == 0 || m[0] != "results" {
		n := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[:1]
		if len(n) == 0 || n[0] != "results" {
			log.Debug("The message doesn't contain results")
		} else {
			attachment := slack.Attachment{
				Text:       "What date do you want the results from?",
				CallbackID: "resultsKudoDate",
				Color:      "#AED6F1",
				Actions: []slack.AttachmentAction{
					{
						Name: "SelectResultsMonth",
						Type: "select",
						Options: []slack.AttachmentActionOption{
							{
								Text:  "January",
								Value: "01",
							},
							{
								Text:  "February",
								Value: "02",
							},
							{
								Text:  "March",
								Value: "03",
							},
							{
								Text:  "April",
								Value: "04",
							},
							{
								Text:  "May",
								Value: "05",
							},
							{
								Text:  "June",
								Value: "06",
							},
							{
								Text:  "July",
								Value: "07",
							},
							{
								Text:  "August",
								Value: "08",
							},
							{
								Text:  "September",
								Value: "09",
							},
							{
								Text:  "October",
								Value: "10",
							},
							{
								Text:  "November",
								Value: "11",
							},
							{
								Text:  "December",
								Value: "12",
							},
						},
					},
					{
						Name: "SelectResultsYear",
						Type: "select",
						Options: []slack.AttachmentActionOption{
							{
								Text:  "2018",
								Value: "2018",
							},
							{
								Text:  "2019",
								Value: "2019",
							},
							{
								Text:  "2020",
								Value: "2020",
							},
						},
					},
				},
			}

			params := slack.PostMessageParameters{
				Attachments: []slack.Attachment{
					attachment,
				},
			}
			_, _, err := s.Client.PostMessage(ev.Channel, "", params)
			if err != nil {
				log.WithError(err).Error("Failed to post help")
			}
			log.WithFields(log.Fields{"userid": ev.User}).Debug("Help message posted")
		}
	}
	return nil
}

func (s *Slack) getKudos(year, month string) []Kudos {
	arg := fmt.Sprintf("%s-%s%s", year, month, "%")
	sql := `SELECT 	from_users.slack_displayname AS from_name,
	kudos.kudo_comment AS kudo_comment,
	kudos.kudo_uuid AS kudo_uuid,
	kudos.kudo_date AS date,
	array_agg(to_users.slack_displayname) AS to_name
	FROM kudos
	INNER JOIN relations ON relations.kudo_uuid = kudos.kudo_uuid
	INNER JOIN users from_users ON kudos.kudo_uuid = relations.kudo_uuid AND from_users.slack_userid = relations.slack_userid_give
	INNER JOIN users to_users ON kudos.kudo_uuid = relations.kudo_uuid AND to_users.slack_userid = relations.slack_userid_receive
	WHERE kudos.kudo_date::text LIKE $1
	GROUP BY kudos.kudo_uuid, date, from_name, kudo_comment`
	rows, err := DB.Queryx(sql, arg)
	if err != nil {
		log.Error(err)
	}
	defer rows.Close()
	kudos := []Kudos{}
	for rows.Next() {
		kudo := Kudos{}
		err := rows.StructScan(&kudo)
		if err != nil {
			log.Error(err)
		}
		kudos = append(kudos, kudo)
	}
	return kudos
}

func (s *Slack) generateDoc(channelid, year, month string) error {

	ss := spreadsheet.New()
	sheet := ss.AddSheet()

	sheet.Cell("A1").SetString("From")
	sheet.Cell("B1").SetString("To")
	sheet.Cell("C1").SetString("Kudos")

	fmt.Sprintln(year)
	fmt.Sprintln(month)

	kudos := s.getKudos(year, month)

	for _, kudo := range kudos {
		row := sheet.AddRow()
		row.AddCell().SetString(kudo.FromName)
		name := kudo.ToName
		name = strings.TrimLeft(name, "{")
		name = strings.TrimRight(name, "}")
		row.AddCell().SetString(name)
		row.AddCell().SetString(kudo.Comment)
	}

	if err := ss.Validate(); err != nil {
		log.Fatalf("error validating sheet: %s", err)
	}
	err := ss.SaveToFile("results.xlsx")
	if err != nil {
		log.WithError(err).Error("Could not save the spreadsheet")
	}

	c := strings.Fields(channelid)
	params := slack.FileUploadParameters{File: "results.xlsx", Channels: c}
	_, err = s.Client.UploadFile(params)
	if err != nil {
		log.WithError(err).Error("Could not Upload the file to slack")
	}

	return nil
}
