package app

import (
	"fmt"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/nlopes/slack"
)

func (s *Slack) askHelp(ev *slack.MessageEvent) error {

	m := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[1:]
	if len(m) == 0 || m[0] != "help" {
		n := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[:1]
		if len(n) == 0 || n[0] != "help" {
			log.Debug("The message doesn't contain help")
		} else {
			attachment := slack.Attachment{
				Pretext: "Welcome to the help center",
				Color:   "#87cefa",
				Text:    "Here are the different commands you use:",
				Fields: []slack.AttachmentField{
					{
						Title: "kudos",
						Value: "The command `kudos` allows you to reward someone with a kudos and to explain why",
					},
				},
				CallbackID: fmt.Sprintf("askHelp_%s", ev.User),
			}

			params := slack.PostMessageParameters{
				Attachments: []slack.Attachment{
					attachment,
				},
			}
			_, _, err := s.Client.PostMessage(ev.Channel, "", params)
			if err != nil {
				log.WithError(err).Error("Failed to post help")
			}
			log.WithFields(log.Fields{"userid": ev.User}).Debug("Help message posted")
		}
	}
	return nil
}
