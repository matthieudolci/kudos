package app

import (
	"context"
	"fmt"
	"os"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/nlopes/slack"
)

// Slack is the primary struct for our slackbot
type Slack struct {
	Name  string
	Token string

	User   string
	UserID string

	Client       *slack.Client
	MessageEvent *slack.MessageEvent
}

// New returns a new instance of the Slack struct, primary for our slackbot
func New() (*Slack, error) {
	token := os.Getenv("SLACK_TOKEN")
	if len(token) == 0 {
		return nil, fmt.Errorf("Could not discover API token")
	}

	return &Slack{Client: slack.New(token), Token: token, Name: "kudos"}, nil
}

// Run is the primary service to generate and kick off the slackbot listener
// This portion receives all incoming Real Time Messages notices from the workspace
// as registered by the API token
func Run(ctx context.Context, s *Slack) error {
	authTest, err := s.Client.AuthTest()
	if err != nil {
		return fmt.Errorf("Did not authenticate: %+v", err)
	}

	s.User = authTest.User
	s.UserID = authTest.UserID

	log.WithFields(log.Fields{"username": s.User, "userid": s.UserID}).Info("Bot is now registered")

	go run(ctx, s)
	return nil

}

func run(ctx context.Context, s *Slack) {

	rtm := s.Client.NewRTM()
	go rtm.ManageConnection()

	log.Info("Now listening for incoming messages...")

	for msg := range rtm.IncomingEvents {
		switch ev := msg.Data.(type) {
		case *slack.MessageEvent:
			if len(ev.User) == 0 {
				continue
			}

			direct := strings.HasPrefix(ev.Msg.Channel, "D")

			if !direct {
				continue
			}

			user, err := s.Client.GetUserInfo(ev.User)
			if err != nil {
				log.WithFields(log.Fields{"userid": ev.User}).Printf("Could not grab user information.")
				continue
			}

			log.WithFields(log.Fields{"username": user.Profile.RealName, "userid": ev.User}).Debug("Received message")

			err = s.giveKudos(ev)
			if err != nil {
				log.WithFields(log.Fields{"username": user.Profile.RealName, "userid": ev.User}).WithError(err).Error("Posting Kudos to user")
			}

			err = s.askHelp(ev)
			if err != nil {
				log.WithFields(log.Fields{"username": user.Profile.RealName, "userid": ev.User}).WithError(err).Error("Posting help message")
			}

			err = s.askForDate(ev)
			if err != nil {
				log.WithFields(log.Fields{"username": user.Profile.RealName, "userid": ev.User}).WithError(err).Error("Posting results message")
			}

		case *slack.RTMError:
			log.Errorf("%s", ev.Error())
		}
	}
}
