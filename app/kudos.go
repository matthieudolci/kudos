package app

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/google/uuid"
	"github.com/nlopes/slack"
)

var uuids []string

func (s *Slack) giveKudos(ev *slack.MessageEvent) error {

	m := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[1:]
	if len(m) == 0 || m[0] != "kudos" {
		n := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[:1]
		if len(n) == 0 || n[0] != "kudos" {
			log.Debug("The message doesn't contain kudos")
		} else {
			u := s.createsUUID()
			uuids = append(uuids, u)

			attachment := slack.Attachment{
				Text:       "Who do you want to reward a kudos to?",
				CallbackID: "choseUser",
				Color:      "#AED6F1",
				Actions: []slack.AttachmentAction{
					{
						Name:       "UserChosen",
						Text:       "Type to filter option",
						Type:       "select",
						DataSource: "users",
					},
					{
						Name:  "CancelKudos",
						Text:  "Cancel",
						Type:  "button",
						Value: "CancelKudos",
						Style: "danger",
					},
				},
			}
			params := slack.PostMessageParameters{
				Attachments: []slack.Attachment{
					attachment,
				},
			}

			_, _, err := s.Client.PostMessage(ev.Channel, "", params)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Slack) createsUUID() string {
	u, err := uuid.NewRandom()
	if err != nil {
		log.WithError(err).Error("Could not generate the uuid")
	}
	result := fmt.Sprint(u)
	return result
}

func (s *Slack) addUUIDToKudos(uuid string) error {
	sql := `
	INSERT INTO kudos (kudo_uuid)
	VALUES ($1)`
	_, err := DB.Exec(sql, uuid)
	if err != nil {
		return err
	}
	return nil
}

func (s *Slack) addUserToUsers(userid, displayname string) error {
	checkID := `SELECT slack_userid FROM users WHERE slack_userid=$1;`
	row := DB.QueryRow(checkID, userid)
	switch err := row.Scan(&userid); err {
	// if user doesnt exit creates it in the database
	case sql.ErrNoRows:
		sql := `
		INSERT INTO users (slack_userid, slack_displayname)
		VALUES ($1, $2)
		RETURNING slack_userid`
		_, err := DB.Exec(sql, userid, displayname)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Slack) addUserAndUUIDToRelations(uuid, useridReceive, useridGive string) error {
	sql := `
	INSERT INTO relations (kudo_uuid, slack_userid_receive, slack_userid_give)
	VALUES ($1, $2, $3)`
	_, err := DB.Exec(sql, uuid, useridReceive, useridGive)
	if err != nil {
		return err
	}
	return nil
}

func (s *Slack) giveMultipleKudos(uuid, userid, channel string) error {

	attachment := slack.Attachment{
		Text:       "Do you want to give this kudos to another person?",
		CallbackID: uuid,
		Color:      "#AED6F1",
		Actions: []slack.AttachmentAction{
			{
				Name:       "AnotherUserChosen",
				Text:       "Type to filter option",
				Type:       "select",
				DataSource: "users",
			},
			{
				Name:  "NoMoreKudos",
				Text:  "Nope",
				Type:  "button",
				Value: "NoMoreKudos",
				Style: "danger",
			},
		},
	}
	params := slack.PostMessageParameters{Attachments: []slack.Attachment{attachment}}

	_, _, err := s.Client.PostMessage(channel, "", params)
	if err != nil {
		return err
	}
	return nil
}

func (s *Slack) askKudosComment(channelid, uuid string) error {

	attachment := slack.Attachment{Text: "What are you giving the kudos for?", CallbackID: uuid}

	params := slack.PostMessageParameters{Attachments: []slack.Attachment{attachment}}
	_, timestamp, err := s.Client.PostMessage(channelid, "", params)
	if err != nil {
		return err
	}

	ticker := time.NewTicker(3 * time.Second)
	timer := time.NewTimer(3 * time.Minute)

loop:
	for {
		select {
		case <-timer.C:
			err := s.kudosCanceled(channelid)
			if err != nil {
				return err
			}
			err = s.cleanKudos(uuid)
			if err != nil {
				return err
			}
			break loop

		case <-ticker.C:
			params := slack.HistoryParameters{Count: 1, Oldest: timestamp}

			history, err := s.Client.GetIMHistory(channelid, params)
			if err != nil {
				log.WithError(err).Error("Error while getting the IM History")
				continue
			}

			message := history.Messages

			if len(message) == 0 {
				continue
			}
			if len(message) > 0 {
				text := history.Messages[0].Msg.Text
				switch text {
				case "cancel":
					err := s.kudosCanceled(channelid)
					if err != nil {
						return err
					}
					err = s.cleanKudos(uuid)
					if err != nil {
						return err
					}
					break loop
				default:
					err := s.registerComment(text, uuid)
					if err != nil {
						return err
					}
					err = s.thanksKudos(channelid, uuid)
					if err != nil {
						return err
					}
					break loop
				}
			}
		}
	}
	return nil
}

func (s *Slack) registerComment(text, uuid string) error {
	sql := `UPDATE kudos SET kudo_comment=$1 WHERE kudo_uuid=$2`
	_, err := DB.Exec(sql, text, uuid)
	if err != nil {
		return err
	}
	return nil
}

func (s *Slack) thanksKudos(channelid, uuid string) error {

	attachment := slack.Attachment{Text: ":tada: Thanks for giving a kudos :tada:", CallbackID: uuid}

	params := slack.PostMessageParameters{Attachments: []slack.Attachment{attachment}}
	_, _, err := s.Client.PostMessage(channelid, "", params)
	if err != nil {
		return err
	}
	return nil
}

func (s *Slack) cleanKudos(uuid string) error {
	sql1 := `DELETE FROM relations WHERE kudo_uuid = $1;`
	sql2 := `DELETE FROM kudos WHERE kudo_uuid = $1;`
	tx, err := DB.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(sql1, uuid)
	if err != nil {
		return err
	}
	_, err = tx.Exec(sql2, uuid)
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

func (s *Slack) kudosCanceled(channelid string) error {
	attachment := slack.Attachment{
		Text:       "The kudos was canceled.\nYou can start a new kudo by sending `kudos`",
		Color:      "#f91b1b",
		CallbackID: "KudosCancel",
	}

	params := slack.PostMessageParameters{
		Attachments: []slack.Attachment{
			attachment,
		},
	}
	_, _, err := s.Client.PostMessage(channelid, "", params)
	if err != nil {
		return err
	}

	return nil
}
