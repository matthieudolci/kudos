TAG ?= latest
BRANCH ?= branch

build:
	go build -installsuffix cgo -o kudos
.PHONY: build

docker:
	docker build -t registry.gitlab.com/matthieudolci/kudos/$(BRANCH)/kudos:$(TAG) .
.PHONY: build-docker

push:
	docker push registry.gitlab.com/matthieudolci/kudos/$(BRANCH)/kudos:$(TAG)
.PHONY: push

lint:
	docker run --rm -t \
	-v ${PWD}:/go/src/gitlab.com/matthieudolci/kudos \
	-w /go/src/gitlab.com/matthieudolci/kudos \
	golangci/golangci-lint run
.PHONY: lint

sec:
	docker run --rm -t \
	-v ${PWD}:/go/src/gitlab.com/matthieudolci/kudos \
	-w /go/src/gitlab.com/matthieudolci/kudos \
	golangci/golangci-lint run -E gosec
.PHONY: sec
